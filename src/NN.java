import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;

public class NN {
	
	public ArrayList<Layer> layers;
	protected int numLayers;
	protected int[] neurons_in_layers;
//	protected String activation_function = "sigmoid";
	protected String activation_function = "softmax";
	public int training_size = 10000;
	
	protected ArrayList<double[]> training_images;
	protected ArrayList<Integer> training_labels;
	protected HashSet<String> allowed_activation_functions;
	
	/**
	 * the length of the array is the number of layers and the numbers in the array corresponds to the 
	 * number of neurons in each layer
	 * @param neurons_in_layers
	 */
	public NN(int[] neurons_in_layers){
		
		if(neurons_in_layers.length < 2){
			throw new RuntimeException("Too few layers");
		}
		
		this.layers = new ArrayList<>();
		this.numLayers = neurons_in_layers.length;
		this.neurons_in_layers = neurons_in_layers;
	}
	
	public NN(int[] neurons_in_layers, String activation_function){
		this(neurons_in_layers);
		this.allowed_activation_functions = new HashSet<>();
		this.allowed_activation_functions.addAll(Arrays.asList(new String[]{"softmax", "sigmoid", "relu", "leaky_relu"}));
		this.activation_function = activation_function;
		
		if(!(allowed_activation_functions.contains(activation_function))){
			System.out.println("Please use one of the following activation functions: softmax, sigmoid, relu, leaky_relu");
			System.out.println("Defaulted to softmax");
			this.activation_function = "softmax";
		}
	}
	
	
	public NN(String folderpath){
		this.layers = new ArrayList<>();
		readNNfromFile(folderpath);
	}
	
	public NN(String folderpath, String activation_function){
		this.layers = new ArrayList<>();
		this.activation_function = activation_function;
		readNNfromFile(folderpath);
	}
	
	public void train_instance(double[] input, double[] correct_output){
		Layer firstLayer = layers.get(0);
		
		firstLayer.feedForward(input);
		//calculate errors in the second last hidden layer
		Layer lastLayer = layers.get(layers.size()-2);
		lastLayer.error = new ArrayRealVector(getErrorsForInstance(correct_output));
		lastLayer.adjust_weights_and_biases();

		//calculate errors in hidden layers through backpropagation
		// adjust the weights and biases
		for(int i = layers.size() - 3; i >= 0; i--){
			Layer layer = layers.get(i);
			layer.calculate_error();
			layer.adjust_weights_and_biases();
		}
		
	}
	
	
	public void train(MnistManager dataset) throws IOException{
		readInDataset(dataset);
		
		Random rd = new Random();
		int pos = 0;
		int right = 0;
			while(pos < training_images.size()){
				int nr = rd.nextInt(training_images.size());
				int correctNeuron = training_labels.get(nr);
				double[] correct_output = new double[10];
				correct_output[correctNeuron] = 1;
				double[] imageAsArray = training_images.get(nr);

				train_instance(imageAsArray, correct_output);
				
				if(correctNeuron == getMostActiveNeuron()){
					right++;
				}
				
				pos++;
			}
			
			System.out.println("Right : " + right);
			System.out.println("Wrong : " + (pos - right));
			
	}
	
	
	public void readInDataset(MnistManager dataset){
		MnistImageFile images = dataset.getImages();
		MnistLabelFile labels = dataset.getLabels();
		
		if(this.training_images == null){
			this.training_images = new ArrayList<>();
			this.training_labels = new ArrayList<>();
			for(int i = 0 ; i < training_size; i++){
				int[][] x;
				try {
					x = images.readImage();
					double[] imageAsArray = MyUtils.flattenMatrix(MyUtils.intToDoubleArray(x));
					training_images.add(imageAsArray);
					training_labels.add(labels.readLabel());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Images have been read in");
		}
	}
	
	
	
	public void evaluateDataset(MnistManager dataset){
		MnistImageFile images = dataset.getImages();
		MnistLabelFile labels = dataset.getLabels();
		System.out.println("Evaluating Testdataset");
		
		int pos = 0;
		int right = 0;
		int[][] image;
		try {
			while(pos < 10000){
				image = images.readImage();
				int correctNeuron = labels.readLabel();
				Layer firstLayer = layers.get(0);
				
				double[] imageAsArray = MyUtils.flattenMatrix(MyUtils.intToDoubleArray(image));
				firstLayer.feedForward(imageAsArray);
				
				if(correctNeuron == getMostActiveNeuron()){
					right++;
				}
				pos++;
			}
			
			System.out.println("Right : " + right);
			System.out.println("Wrong : " + (pos - right));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public double getCostForDataset(MnistManager dataset){
		double cost = 0;
		
		MnistImageFile images = dataset.getImages();
		MnistLabelFile labels = dataset.getLabels();
		
		
		int pos = 0;
		int[][] image;
		try {
			while(pos < 1000){
//			while((image = images.readImage()) != null){
				image = images.readImage();
				int correctNeuron = labels.readLabel();
				double[] correct_output = correct_output_from_neuron(correctNeuron);
				Layer firstLayer = layers.get(0);
				
				double[] imageAsArray = MyUtils.flattenMatrix(MyUtils.intToDoubleArray(image));
				
				firstLayer.feedForward(imageAsArray);
				
				cost += getCostForInstance(correct_output);
				
				pos++;
			}
//			System.out.println("Cost calculated");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return cost;
	}
	
	
	
	
	/**
	 * returns the most active neuron in the last layer, given the input
	 * @param input
	 * @return
	 */
	public int evaluateInstance(double[] input){
		Layer firstLayer = layers.get(0);
		firstLayer.feedForward(input);
		
		Layer lastLayer = layers.get(layers.size()-1);
		
		return lastLayer.getMostActiveNeuron();
	}

	
	
	public double[] getErrorsForInstance(double[] correct_output){
		Layer lastLayer = layers.get(layers.size()-1);
		double[] a = lastLayer.a.toArray();
		
		double[] errors = new double[a.length];
		
		for(int i = 0; i < lastLayer.getNumNeurons(); i++){
			errors[i] = correct_output[i] - a[i];
//			errors[i] = Math.pow(a[i] - correct_output[i], 2);		// the cost function is different from the error function
			// i believe the error function is the derivative of the cost function? 
		}
		
		return errors;
	}
	
	

	
	/**
	 * feedforward has to be called before this method
	 * @param correctNeuron
	 * @return
	 */
	public double getCostForInstance(double[] correct_output){
		double cost = 0;
		double[] errors = getErrorsForInstance(correct_output);
		
		for(double error : errors){
			cost += error;
		}
		
		return cost;
	}
	
	

	
	public void initLayersRandomly(){
		
		Layer previousLayer = null;
		for(int i = 0; i < numLayers-1; i++){
			Layer layer = new Layer(neurons_in_layers[i], neurons_in_layers[i+1], this.activation_function);
			layers.add(layer);

			if(previousLayer != null){
				previousLayer.setNextLayer(layer);
			}
			
			previousLayer = layer;
			
		}
		
		Layer lastLayer = new Layer(neurons_in_layers[numLayers-1]);
		lastLayer.isLastLayer = true;
		lastLayer.activation_function = this.activation_function;
		previousLayer.setNextLayer(lastLayer);
		layers.add(lastLayer);	// add last layer
	}
	
	
	public NN getMutatedNN(){
		NN mutatedNN = new NN(neurons_in_layers);
		
		Layer previousLayer = null;

		for(int i = 0; i < numLayers-1; i++){
			Layer layer = layers.get(i);
			
			Layer mutatedLayer = layer.getMutatedLayer();
			
			mutatedNN.layers.add(mutatedLayer);

			if(previousLayer != null){
				previousLayer.setNextLayer(mutatedLayer);
			}
			
			previousLayer = mutatedLayer;
		}
		
		
		Layer lastLayer = layers.get(numLayers-1);
		Layer mutatedLastLayer = lastLayer.getMutatedLayer();
		previousLayer.setNextLayer(mutatedLastLayer);
		mutatedNN.layers.add(mutatedLastLayer);	// add last layer
		
		
		return mutatedNN;
	}
	
	public static double[] correct_output_from_neuron(int correctNeuron){
		double[] correct_output = new double[10];
		correct_output[correctNeuron] = 1;
		return correct_output;
	}
	
	
	public int getMostActiveNeuron(){
		return layers.get(layers.size()-1).getMostActiveNeuron();
	}

	
	/**
	 * returns the result content of the last layer after feeding in an instance
	 * @param input
	 * @return
	 */
	public double[] process_instance(double[] input){
		Layer lastLayer = layers.get(layers.size()-1);
		Layer firstLayer = layers.get(0);
		
		firstLayer.feedForward(input);
		
		
		System.out.println(lastLayer.a);
		return lastLayer.a.toArray();
	}
	
	
	public void readNNfromFile(String folderpath){
		String biasesFile = folderpath + "/biases.txt";
		String weightsFile = folderpath + "/weights.txt";
		
		readBiasFile(biasesFile);
		readWeightsFile(weightsFile);
	}
	
	
	public void readWeightsFile(String weightsFile){
		try {
			BufferedReader weights_br = new BufferedReader(new FileReader(weightsFile));
			String line;
			ArrayList<double[]> matrix_rows = new ArrayList<>();
			int current_matrix = 0;
			while((line = weights_br.readLine()) != null){
				if(line.equals("")){
					double[][] weights = new double[matrix_rows.size()][matrix_rows.get(0).length];
					for(int i = 0; i<weights.length; i++){
						for(int j = 0; j<weights[0].length; j++){
							weights[i][j] = matrix_rows.get(i)[j];
//							System.out.println(weights[i][j]);
						}
					}
					this.layers.get(current_matrix).weights = new Array2DRowRealMatrix(weights);
					this.layers.get(current_matrix).a = new ArrayRealVector(matrix_rows.get(0).length);
					
					matrix_rows.clear();
					current_matrix++;
					if(current_matrix >= this.numLayers-1){		//needed, because there are multiple empty lines at the 
						break;									//end of the file and the program would try to create 
					}											//empty matrices
				}
				else{
					String[] numbers = line.split("; ");
					double[] weights_row = Arrays.stream(numbers).mapToDouble(Double::parseDouble).toArray();
					matrix_rows.add(weights_row);
				}
			}
			
			weights_br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void readBiasFile(String biasesFile){
		try {
			BufferedReader bias_br = new BufferedReader(new FileReader(biasesFile));
			String line;
			int lines = 0;
			while((line = bias_br.readLine()) != null){
				if(!(line.equals(""))){
					lines++;
				}
			}
			bias_br.close();
			this.numLayers = lines + 1;
			
			
			bias_br = new BufferedReader(new FileReader(biasesFile));
			ArrayList<double[]> layer_biases = new ArrayList<>();
			while((line = bias_br.readLine()) != null){
				if(!(line.equals(""))){
					line = line.replace(',', '.');
					String[] numbers = line.split("; ");
					double[] biases = Arrays.stream(numbers).mapToDouble(Double::parseDouble).toArray();
					layer_biases.add(biases);
				}
			}
			
			
			Layer previousLayer = null;
			for(int i = 0; i < numLayers-1; i++){
				Layer layer = new Layer(layer_biases.get(i).length, this.activation_function);
				layers.add(layer);
				layer.bias = new ArrayRealVector(layer_biases.get(i));

				if(previousLayer != null){
					previousLayer.setNextLayer(layer);
				}
				previousLayer = layer;
			}
			
			Layer lastLayer = new Layer(layer_biases.get(numLayers-2).length, this.activation_function);
			lastLayer.isLastLayer = true;
			previousLayer.setNextLayer(lastLayer);
			layers.add(lastLayer);	// add last layer
			bias_br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveNN(String folderpath){
		String biasesFile = folderpath + "/biases.txt";
		String weigthsFile = folderpath + "/weights.txt";
		
		try {
			BufferedWriter bias_bw = new BufferedWriter(new FileWriter(biasesFile));
			BufferedWriter weight_bw = new BufferedWriter(new FileWriter(weigthsFile));
			
			for(Layer layer : this.layers){
				bias_bw.write(layer.getBiases() + "\n");
				weight_bw.write(layer.getWeights() + "\n");
			}
			bias_bw.flush();
			bias_bw.close();
			weight_bw.flush();
			weight_bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public String toString(){
		String output = "";
		int i = 0;
		for(Layer layer : layers){
			output += "Layer " + i++ + ": " + layer.toString() + "\n";
		}
		return output;
	}
}
