import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ImageDisplayer extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int height = 400;
	private int width = 800;
	
	public ImageDisplayer(BufferedImage img, String label){
		Image image = img.getScaledInstance(84, 84, 0);
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(new JLabel(new ImageIcon(image)));
		this.getContentPane().add(new JLabel(label));
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(width, height);
		
		
	}
	
	public ImageDisplayer(){
		
	}
	
	
	public void displayImage(BufferedImage img, String label){
		Image image = img.getScaledInstance(84, 84, 0);
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(new JLabel(new ImageIcon(image)));
		this.getContentPane().add(new JLabel(label));
		this.pack();
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(width, height);
		this.setVisible(true);
	}
	
	public void display(){
		this.setVisible(true);
	}
}
