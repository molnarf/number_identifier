import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

public class MyUtils {

	
	
	public static double[][] intToDoubleArray(int[][] arr){
		double[][] output = new double[arr.length][arr[0].length];
		
		for(int i = 0; i<arr.length; i++){
			for(int j = 0; j<arr[0].length; j++){
				output[i][j] = arr[i][j];
			}
		}
		
		return output;
	}
	
	
	public static void saveArrayAsPNG(int[][] arr, String path){
		int xLenght = arr.length;
		int yLength = arr[0].length;
		BufferedImage b = new BufferedImage(xLenght, yLength, BufferedImage.TYPE_INT_RGB);

		for (int x = 0; x < xLenght; x++) {
			for (int y = 0; y < yLength; y++) {
				int rgb = (int) arr[y][x] << 16 | (int) arr[y][x] << 8 | (int) arr[y][x];
				b.setRGB(x, y, rgb);
			}
		}

		try {
			ImageIO.write(b, "png", new File(path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static BufferedImage arrayToImage(int[][] arr){
		int xLenght = arr.length;
		int yLength = arr[0].length;
		BufferedImage b = new BufferedImage(xLenght, yLength, BufferedImage.TYPE_INT_RGB);

		for (int x = 0; x < xLenght; x++) {
			for (int y = 0; y < yLength; y++) {
				int rgb = (int) arr[y][x] << 16 | (int) arr[y][x] << 8 | (int) arr[y][x];
				b.setRGB(x, y, rgb);
			}
		}
		
		return b;
	}
	
	
	public static double[] flattenMatrix(double[][] matrix){
		double[] array = new double[matrix.length * matrix[0].length];
		for(int i = 0; i < matrix.length; i++){
			for(int j = 0; j < matrix[0].length; j++){
				int arrayPos = i * matrix.length + j;
				array[arrayPos] = matrix[i][j];
			}
		}
		
		return array;
	}
	
	
	public static String arrayToString(double[] array){
		String output = "";
		
		for(int i = 0; i<array.length; i++){
			output += array[i] + "  ";
		}
		
		return output;
	}
	
	public static String arrayToString(int[][] array){
		String output = "";
		for(int i = 0; i<array.length; i++){
			for(int j = 0; j<array[0].length; j++){
				output += array[i][j] + " ";
			}
			output += "\n";
		}
		return output;
	}
	
	
	public static double[] mutateArray(double[] array){
		Random rd = new Random();
		double[] mutatedArray = new double[array.length];
		
		for(int i = 0; i < array.length; i++){
			if(rd.nextBoolean()){
				mutatedArray[i] = array[i] + ((rd.nextDouble() - 0.5) * 1.1);
			}
			else{
				mutatedArray[i] = array[i] - ((rd.nextDouble() - 0.5) * 1.1);
			}
		}
		
		return mutatedArray;
	}
	
	
	public static double[][] mutateArray(double[][] array){
		Random rd = new Random();
		double[][] mutatedArray = new double[array.length][array[0].length];
		
		for(int i = 0; i < array.length; i++){
			for(int j = 0; j < array[0].length; j++){
				if(rd.nextBoolean()){
					mutatedArray[i][j] = array[i][j] + ((rd.nextDouble() - 0.5) * 1.1);
				}
				else{
					mutatedArray[i][j] = array[i][j] - ((rd.nextDouble() - 0.5) * 1.1);
				}
			}
		}
		
		return mutatedArray;
	
	}
	
	
	
	public static double[] normalizeInput(double[] input){
		 double sum = 0.0;
		 double sd = 0.0;
	     int length = input.length;

        for(double num : input) {
            sum += num;
        }

        double mean = sum/length;

        for(double num : input) {
            sd += Math.pow(num - mean, 2);
        }
        sd = Math.sqrt(sd/length);
	
        for(int i = 0; i<input.length; i++){
        	input[i] = (input[i] - mean) / sd;
        }
        
	    return input;
	}
	
	
	
	public static RealVector softmax(RealVector input){
		double[] result = new double[input.getDimension()];
		double sum = 0;
		for(int i = 0; i<result.length; i++){
			sum += Math.exp(input.getEntry(i));
		}
		for(int i = 0; i<result.length; i++){
			result[i] = Math.exp(input.getEntry(i)) / sum;
		}
		
		return new ArrayRealVector(result);
	}
	
	
	/**
	 * softmax�(x) = softmax(x) * (1 - softmax)
	 * @param input
	 * @return
	 */
	public static RealVector derivative_softmax(RealVector input){
		RealVector derivative = softmax(input);
		RealVector neg_derivative_plus_one = derivative.mapMultiply(-1).mapAddToSelf(1);
		derivative = neg_derivative_plus_one.ebeMultiply(derivative);

		return derivative;
	}
	
	
	public static double relu(double x){
		return Math.max(0, x);
	}
	
	public static double derivative_relu(double x){
		if(x <= 0){
			return 0;
		}
		else{
			return 1;
		}
	}
	
	public static double leaky_relu(double x){
		if(x > 0){
			return x;
		}
		else{
			return 0.01 * x;
		}
	}
	
	public static double derivative_leaky_relu(double x){
		if( x > 0){
			return 1;
		}
		else{
			return -0.01;
		}
	}
	
	public static double sigmoid(double x){
		 return 1.0 / (1.0 + Math.exp(-x));
	}
	
	public static double derivative_sigmoid(double x){
		return sigmoid(x) * (1 - sigmoid(x));
	}
	
	
	
	public static RealVector derivative_sigmoid(RealVector output){
		RealVector derivative = output.mapMultiply(-1);
		derivative = derivative.mapAdd(1);
		derivative = output.ebeMultiply(derivative);
		
		return derivative;
	}

	
	public static double[][] addMatrices(double[][] m1, double[][] m2){
		if(m1.length != m2.length || m1[0].length != m2[0].length){
			throw new RuntimeException("The size of the matrices does not match");
		}
		
		double[][] result = new double[m1.length][m1[0].length];
		
		for(int i = 0; i<m1.length; i++){
			for(int j = 0; j<m1[0].length;j++){
				result[i][j] = m1[i][j] + m2[i][j];
			}
		}
		
		return result;
	}
	
	public static double[][] mult_Mat_with_double(double[][] m1, double learning_rate){
		double[][] result = new double[m1.length][m1[0].length];
		
		for(int i = 0; i<m1.length; i++){
			for(int j = 0; j<m1[0].length;j++){
				result[i][j] = m1[i][j] * learning_rate;
			}
		}
		
		return result;
	}
	
	
	public static double[][] mult_Mat_with_Mat(double[][] m1, double[][] m2){
		if(m1.length != m2.length || m1[0].length != m2[0].length){
			throw new RuntimeException("The size of the matrices does not match");
		}
		
		double[][] result = new double[m1.length][m1[0].length];
		
		for(int i = 0; i<m1.length; i++){
			for(int j = 0; j<m1[0].length;j++){
				result[i][j] = m1[i][j] * m2[i][j];
			}
		}
		
		return result;
	}
	

	public static String arrayToString(double[][] array) {
		String output = "";
		for(int i = 0; i<array.length; i++){
			for(int j = 0; j<array[0].length; j++){
				output += (array[i][j]) + " ";
//				output += Math.round(array[i][j]) + " ";
			}
			output += "\n";
		}
		return output;
	
	}

	
	public static double[][] copyArray(double[][] array){
		double[][] newArray = new double[array.length][array[0].length];
		for(int i = 0; i<array.length; i++){
			for(int j = 0; j<array[0].length; j++){
				newArray[i][j] = array[i][j];
			}
		}
		
		return newArray;
	}
	
	
	
	public static ArrayList<double[][]> copyDoubleArrayList(ArrayList<double[][]> list){
		ArrayList<double[][]> newList = new ArrayList<>();
		for(double[][] array : list){
			newList.add(copyArray(array));
		}
		
		return newList;
	}
	
	
	
}
