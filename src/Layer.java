import org.apache.commons.math3.distribution.NormalDistribution;

import org.apache.commons.math3.linear.*;


public class Layer {
	
	
	public RealMatrix weights;
	public RealVector a;	//input
	public RealVector bias;

	public RealVector output;
	public RealVector error;
	
	private Layer nextLayer;
	public boolean isLastLayer = false;
											//  0.001 works well for sigmoid
	private double learning_rate = 0.00001;   //  0.00001 works well for leaky_relu
//	public String activation_function = "sigmoid";
	public String activation_function = "softmax";
	public boolean normalize = false;	//note normalization leads to worse performance (at least for number identification)
	
	private int numNeurons;
	private int numNeuronsNextLayer;
	
	/*		 # Neurons
	 * 		-------------
	 * 		| | | | | | |	
	 * 		-------------	# neurons next layer
	 * 		| | | | | | |	
	 * 		-------------
	 * 		| | | | | | |
	 * 		-------------
	 */
	
	
	public Layer(int numNeurons, int numNeuronsNextLayer){
		this.a = new ArrayRealVector(numNeurons);
		this.bias = new ArrayRealVector(numNeuronsNextLayer);
		
		this.weights = new Array2DRowRealMatrix(new double[numNeuronsNextLayer][numNeurons]);
		
		this.numNeurons = numNeurons;
		this.numNeuronsNextLayer = numNeuronsNextLayer;

		initRandomly();
	}
	
	public Layer(int numNeurons, int numNeuronsNextLayer, String activation_function){
		this(numNeurons, numNeuronsNextLayer);
		this.activation_function = activation_function;
	}
	
	
	//last layer or when read in from file
	public Layer(int numNeurons){
		this.a = new ArrayRealVector(numNeurons);
		this.numNeurons = numNeurons;
	}
	
	
	//last layer or when read in from file
	public Layer(int numNeurons, String activation_function){
		this(numNeurons);
		this.activation_function = activation_function;
	}
	
	
	public void feedForward(double[] input){	// input are the pixels of a picture for example
		if(normalize){
			input = MyUtils.normalizeInput(input);	// normalize input images
		}
		this.a = new ArrayRealVector(input);
		if (this.weights != null) {	// the last layer doesnt need to feed forward
			// a+1 = activation_function( weights * a + bias )
			RealVector AnextLayer = this.weights.operate(this.a);	// simple multiplication
			AnextLayer = AnextLayer.add(this.bias);
			AnextLayer = applyActivationFunction(AnextLayer);
			
			this.output = AnextLayer;
			nextLayer.feedForward(AnextLayer.toArray());
		}
	}
	
	
	public void initRandomly(){
		NormalDistribution norm = new NormalDistribution(0, 1.0/numNeurons);
		
		if(this.weights != null){		// weigths is null if its the last layer
			for(int i = 0; i < numNeurons; i++){
				for(int j = 0; j < numNeuronsNextLayer; j++){
					weights.setEntry(j, i, norm.sample());
				}
			}
		}
		// irrelevant for the last layer
		for(int i = 0; i < numNeuronsNextLayer; i++){
			this.bias.setEntry(i, 0);
		}
		
	}
	
	
	public void calculate_error(){
		RealMatrix error_next_layer = new Array2DRowRealMatrix(nextLayer.error.toArray());
		RealMatrix weights_next_layer = nextLayer.weights.copy();

		this.error = weights_next_layer.transpose().multiply(error_next_layer).getColumnVector(0);
		
	}
	
	
	/**
	 * new weights = learn_rate * error * d_relu * a^T
	 *                                           ^ matrix multiplication here
	 * d_relu is the vector of the partial derivatives of the relu functions 
	 */
	public void adjust_weights_and_biases(){
		
		RealVector derivative = new ArrayRealVector(error.getDimension());
		derivative = calculateDerivative(derivative);	//changes the input Vector and applies the derivative of the activation function
		
		RealVector delta_bias =    this.error.mapMultiply(learning_rate).ebeMultiply(derivative);
		RealMatrix delta_weights = this.error.mapMultiply(learning_rate).ebeMultiply(derivative).outerProduct(a);
		
		this.weights = this.weights.add(delta_weights);	
		this.bias = this.bias.add(delta_bias);
	}
	
	
	
	
	
	
	
	
	
	// for genetic networks :
	
	
	public int getMostActiveNeuron(){
		double[] neurons = this.a.toArray();
		
		int mostActive = 0;
		double mostActiveValue = 0;
		
		for(int i = 0; i < numNeurons; i++){
			if(neurons[i] > mostActiveValue){
				mostActiveValue = neurons[i];
				mostActive = i;
			}
		}
		
		return mostActive;
	}
	
	
	public Layer getMutatedLayer(){
		Layer mutatedLayer = null;
		if(this.weights != null){
			mutatedLayer = new Layer(this.numNeurons, this.numNeuronsNextLayer);
			double[][] w = this.weights.getData();
			w = MyUtils.mutateArray(weights.getData());
			mutatedLayer.weights = new Array2DRowRealMatrix(w);
		}
		
		if(mutatedLayer == null){
			mutatedLayer = new Layer(this.numNeurons);
		}
		
		double[] b = this.bias.toArray();
		b = MyUtils.mutateArray(b);
		mutatedLayer.bias = new ArrayRealVector(b);
		
		
		return mutatedLayer;
	}
	
	
	
	public RealVector applyActivationFunction(RealVector AnextLayer){
		if(this.activation_function.equals("softmax")){
			RealVector output = MyUtils.softmax(AnextLayer);
			return output;
		}
		
		for (int i = 0; i < AnextLayer.getDimension(); i++) {
			if(this.activation_function.equals("sigmoid") || nextLayer.isLastLayer){
				AnextLayer.setEntry(i, MyUtils.sigmoid(AnextLayer.getEntry(i)));	//sigmoid can be used instead of relu
			}
			else{
				if(this.activation_function.equals("relu")){
				AnextLayer.setEntry(i, MyUtils.relu(AnextLayer.getEntry(i)));
				}
				if(this.activation_function.equals("leaky_relu")){
					AnextLayer.setEntry(i, MyUtils.leaky_relu(AnextLayer.getEntry(i)));
				}
			}
		}
		
		return AnextLayer;
	}
	
	
	public RealVector calculateDerivative(RealVector derivative){
		if(this.activation_function.equals("softmax") || nextLayer.isLastLayer){
			derivative = MyUtils.derivative_softmax(output);
		}
		if(this.activation_function.equals("sigmoid") || nextLayer.isLastLayer){
			derivative = MyUtils.derivative_sigmoid(output);	// i use sigmoid here 
		}
		else{
			for(int i = 0; i < derivative.getDimension(); i++){
				if(this.activation_function.equals("relu")){
				derivative.setEntry(i, MyUtils.derivative_relu(output.getEntry(i)));
				}
				if(this.activation_function.equals("leaky_relu")){
					derivative.setEntry(i, MyUtils.derivative_leaky_relu(output.getEntry(i)));
				}
			}
		}
		
		return derivative;
	}
	
	
	
	public void setNextLayer(Layer nextLayer){
		this.nextLayer = nextLayer;
	}
	
	public int getNumNeurons(){
		return this.numNeurons;
	}
	
	
	public String getBiases(){
		if(this.bias != null){
			String output = bias.toString();
			return output.substring(1, output.length()-1);
		}
		return "";
	}
	
	
	public String getWeights(){
		if(this.weights != null){
			double[][] matrix = this.weights.getData();
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i<matrix.length; i++){
				for(int j = 0; j<matrix[0].length; j++){
					sb.append(matrix[i][j] + "; ");
				}
				sb.append("\n");
			}
			return sb.toString();
		}
		return "";
	}
	
	public String toString(){
		String output = "a (input) : " + a.toString() + "\n";
		output += "#Neurons : " + this.numNeurons + "\n";
		
		if(this.weights != null && this.bias != null){
//			output += "Weights : " + weights.toString() + "\n";			
			output += "Bias : " + bias.toString() + "\n";
		}
		return output;
	}

}
