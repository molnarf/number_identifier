import java.util.ArrayList;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class Pooling_Layer {
	
	public Conv_Layer nextConvLayer;
	public Layer nextLayer;
	public Conv_Layer previousLayer;
	public int size; // size of the pooling window	
	public int outputsize;
	
	public int numMaps;			// all set by the setNextPoolingLayer method in the Colv_Layer class
	public int h_input;
	public int w_input;
	public int h_output;
	public int w_output;
	
	
//	ArrayList<double[][]> a;
	ArrayList<double[][]> weights; // which cells inside the pooling window have the max values
	ArrayList<RealMatrix> weights_matrix;
	
//	ArrayList<double[][]> output;
	ArrayList<double[][]> errors;
	ArrayList<RealMatrix> errors_matrix;
	
	public Pooling_Layer(int window_size){
		this.size = window_size;
	}
	
	
	public void feedForward(ArrayList<double[][]> featureMaps){
//		this.a = MyUtils.copyDoubleArrayList(featureMaps);			//wird nicht gebraucht
		this.weights = new ArrayList<>();
		this.numMaps = featureMaps.size();
		
		for(int i = 0; i<featureMaps.size(); i++){
			double[][] featureMap = maxPooling(featureMaps.get(i));
			featureMaps.set(i, featureMap);
		}
		
//		this.output = MyUtils.copyDoubleArrayList(featureMaps);		//wird nicht gebraucht
		
		if(this.nextConvLayer != null){
			nextConvLayer.feedForward(featureMaps);
		}
		else{	//flatten output
			int h = featureMaps.get(0).length;
			int w = featureMaps.get(0)[0].length;
			int numMaps = featureMaps.size();
			double[] flattenedFeatures = new double[numMaps*h*w];

			for(int f = 0; f<numMaps; f++){
				for(int i = 0; i<h; i++){
					for(int j = 0; j<w; j++){
						flattenedFeatures[f*h*w + (i*w) + j] = featureMaps.get(f)[i][j];
					}
				}
			}
			
			nextLayer.feedForward(flattenedFeatures);
		}
		
	}

	
	public void feedForward_matrix(ArrayList<RealMatrix> featureMaps){
		this.weights_matrix = new ArrayList<>();
		this.numMaps = featureMaps.size();
		
		for(int i = 0; i<featureMaps.size(); i++){
			RealMatrix featureMap = maxPooling_matrix(featureMaps.get(i));
			featureMaps.set(i, featureMap);
		}
		
		if(this.nextConvLayer != null){
			nextConvLayer.feedForward_matrix(featureMaps);
		}
		else{	//flatten output
			int h = featureMaps.get(0).getRowDimension();
			int w = featureMaps.get(0).getColumnDimension();
			int numMaps = featureMaps.size();
			double[] flattenedFeatures = new double[numMaps*h*w];

			for(int f = 0; f<numMaps; f++){
				for(int i = 0; i<h; i++){
					for(int j = 0; j<w; j++){
						flattenedFeatures[f*h*w + (i*w) + j] = featureMaps.get(f).getEntry(i, j);
					}
				}
			}
			
			nextLayer.feedForward(flattenedFeatures);
		}
		
	}
	
	
	public double[][] maxPooling(double[][] featureMap){
		if(featureMap.length % size != 0 || featureMap[0].length % size != 0){
			throw new RuntimeException("Please make sure the feature map is divideable by the pooling window size \n"
					+ "feature map dims: " + featureMap.length + "x" + featureMap[0].length + "    window size: " + size);
		}
		
		double[][] res = new double[featureMap.length/size][featureMap[0].length/size];
		double[][] weight = new double[featureMap.length][featureMap[0].length];
		
		for(int i = 0; i<featureMap.length; i+=size){
			for(int j = 0; j<featureMap[0].length; j+=size){
				double max = 0;
				int arg_max_ia = 0;
				int arg_max_jb = 0;
				for(int a = 0; a<size; a++){
					for(int b = 0; b<size; b++){
						if(featureMap[i+a][j+b] > max){
							max = featureMap[i+a][j+b];
							arg_max_ia = i+a;
							arg_max_jb = j+b;
						}
					}
				}
				res[i/size][j/size] = max;
				weight[arg_max_ia][arg_max_jb] = 1;
			}
		}
		
		this.weights.add(weight);
		return res;
	}
	
	
	public RealMatrix maxPooling_matrix(RealMatrix featureMap){
		if(featureMap.getRowDimension() % size != 0 || featureMap.getColumnDimension() % size != 0){
			throw new RuntimeException("Please make sure the feature map is divideable by the pooling window size \n"
					+ "feature map dims: " + featureMap.getRowDimension() + "x" + featureMap.getColumnDimension() + "    window size: " + size);
		}
		
		double[][] res = new double[featureMap.getRowDimension()/size][featureMap.getColumnDimension()/size];
		double[][] weight = new double[featureMap.getRowDimension()][featureMap.getColumnDimension()];
		
		for(int i = 0; i<featureMap.getRowDimension(); i+=size){
			for(int j = 0; j<featureMap.getColumnDimension(); j+=size){
				double max = 0;
				int arg_max_ia = 0;
				int arg_max_jb = 0;
				for(int a = 0; a<size; a++){
					for(int b = 0; b<size; b++){
						if(featureMap.getEntry(i+a, j+b) > max){
							max = featureMap.getEntry(i+a, j+b);
							arg_max_ia = i+a;
							arg_max_jb = j+b;
						}
					}
				}
				res[i/size][j/size] = max;
				weight[arg_max_ia][arg_max_jb] = 1;
			}
		}
		
		this.weights_matrix.add(new Array2DRowRealMatrix(weight));
		return new Array2DRowRealMatrix(res);
	}

	
	
	
	public void calculate_error(){
		this.errors = new ArrayList<>();
		
		if(nextLayer != null){
			RealMatrix error_next_layer = new Array2DRowRealMatrix(nextLayer.error.toArray());
			RealMatrix weights_next_layer = nextLayer.weights.copy();
			RealVector error = weights_next_layer.transpose().multiply(error_next_layer).getColumnVector(0);
			
			double[] error_array = error.toArray();
			
			//deflatten the error array of the next layer and calculate the own error
			for(int m = 0; m<this.numMaps; m++){
				double[][] error_matrix = new double[h_input][w_input];
				for(int i = m*h_output*w_output; i<(m+1)*h_output*w_output; i++){
					int pooled_i = (i%(h_output*w_output))/w_output;
					int pooled_j = (i%(h_output*w_output))%w_output;
					for(int a = 0; a<this.size; a++){	//size of pooling window
						for(int b = 0; b<this.size; b++){
							error_matrix[pooled_i*2+a][pooled_j*2+b] = this.weights.get(m)[pooled_i*2+a][pooled_j*2+b] * error_array[i];
						}
					}
				}
				this.errors.add(error_matrix);
			}
		}
		else{	//if the next layer is a pooling layer
			for(int m = 0; m<this.nextConvLayer.errors.size(); m++){
				double[][] error_nextLayer = this.nextConvLayer.errors.get(m);
				double[][] error = new double[h_input][w_input];
				
				for(int i = 0; i<error_nextLayer.length; i++){
					for(int j = 0; j<error_nextLayer[0].length; j++){
						for(int a = i*size; a<(i+1)*size; a++){
							for(int b = j*size; b<(j+1)*size; b++){
								error[a][b] = error_nextLayer[i][j] * this.weights.get(m)[a][b];
							}
						}
					}
				}
				
				this.errors.add(error);
			}
			
		}

	}
	
	
	public void calculate_error_matrix(){
		this.errors_matrix = new ArrayList<>();
		
		if(nextLayer != null){
			RealMatrix error_next_layer = new Array2DRowRealMatrix(nextLayer.error.toArray());
			RealMatrix weights_next_layer = nextLayer.weights.copy();
			RealVector error = weights_next_layer.transpose().multiply(error_next_layer).getColumnVector(0);
			
			double[] error_array = error.toArray();
			
			//deflatten the error array of the next layer and calculate the own error
			for(int m = 0; m<this.numMaps; m++){
				double[][] error_matrix = new double[h_input][w_input];
				for(int i = m*h_output*w_output; i<(m+1)*h_output*w_output; i++){
					int pooled_i = (i%(h_output*w_output))/w_output;
					int pooled_j = (i%(h_output*w_output))%w_output;
					for(int a = 0; a<this.size; a++){	//size of pooling window
						for(int b = 0; b<this.size; b++){
							error_matrix[pooled_i*2+a][pooled_j*2+b] = this.weights.get(m)[pooled_i*2+a][pooled_j*2+b] * error_array[i];
						}
					}
				}
				this.errors_matrix.add(new Array2DRowRealMatrix(error_matrix));
			}
		}
		else{	//if the next layer is a conv layer
			for(int m = 0; m<this.nextConvLayer.errors_matrix.size(); m++){
				RealMatrix error_nextLayer = this.nextConvLayer.errors_matrix.get(m);
				RealMatrix error = new Array2DRowRealMatrix(new double[h_input][w_input]);
				
				for(int i = 0; i<error_nextLayer.getRowDimension(); i++){
					for(int j = 0; j<error_nextLayer.getColumnDimension(); j++){
						for(int a = i*size; a<(i+1)*size; a++){
							for(int b = j*size; b<(j+1)*size; b++){
								error.setEntry(a, b, error_nextLayer.getEntry(i, j)*this.weights_matrix.get(m).getEntry(a, b));
							}
						}
					}
				}
				
				this.errors_matrix.add(error);
			}
			
		}

	}
	
	public int calculateOutputsize(){
		this.outputsize = (h_input * w_input * numMaps)/(size*size);
		return this.outputsize;
	}
	
	
	public void setNextConvLayer(Conv_Layer conv_layer){
		this.nextConvLayer = conv_layer;
		conv_layer.previousLayer = this;
		nextConvLayer.h_input = this.h_output;
		nextConvLayer.w_input = this.w_output;
		nextConvLayer.initialize_biases();
		nextConvLayer.initialize_filters();
	}
}
