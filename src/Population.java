import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


public class Population {

	public ArrayList<NN> networks;
	
	public int numNetworks;
	public int numGen = 1;
	public int numPlayersAlive;
	
	public MnistManager dataset;
	private NN currentBestNN;
	
	public double currentCost;
//	public double learnRate = 0.1;
	
	
	public Population(int numNetworks, MnistManager dataset, int[] neurons_in_layers){
		this.networks = new ArrayList<>();
		this.numNetworks = numNetworks;
		this.dataset = dataset;
		this.currentCost = Double.MAX_VALUE;
		
		for(int i = 0; i < numNetworks; i++){
			NN nn = new NN(neurons_in_layers);
			
			networks.add(nn);
		}
	
	}
	
	
	public void update(){
		double oldCost = currentCost;
		double costDifference = Double.MAX_VALUE;
		
		while(costDifference > 5){
			createOffspring();
			
			costDifference = oldCost - currentCost;
			oldCost = currentCost;
			
			System.out.println("Current cost " + currentCost + "\nCost difference " + costDifference);
		}
		
	}

	
	
	/**
	 * sets the best network in this generation and saves its cost
	 */
	public void determineBestNetwork(){
		NN bestNetwork = networks.get(0);
		try {
			MnistManager dataset = new MnistManager(
					"C:/Users/Florian/Documents/Testdatein/NN images/train-images-idx3-ubyte/train-images.idx3-ubyte",
					"C:/Users/Florian/Documents/Testdatein/NN images/train-labels-idx1-ubyte/train-labels.idx1-ubyte");

		for(NN nn : networks){
			if(bestNetwork.getCostForDataset(dataset) > nn.getCostForDataset(dataset)){
				bestNetwork = nn;
				this.currentCost = bestNetwork.getCostForDataset(dataset);
			}
		}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.currentBestNN = bestNetwork;
	}
	
	
	/**
	 * determines the best network and creates its offspring
	 */
	public void createOffspring(){
		System.out.println("Determining best network");
		determineBestNetwork();
		System.out.println("Best network determined");
		
		this.numGen++;
		
		this.networks.clear();
//		addRandomPlayer(100);
		
		for(int i = 0; i < numNetworks; i++){
			NN mutatedNN = currentBestNN.getMutatedNN();
			networks.add(mutatedNN);
		}
		
		networks.add(currentBestNN);
	}
	
//	---------------------------------------------------------------------------------------------------------------

	
	
	public double[] mutateArray(double[] array){
		Random rd = new Random();
		double[] mutatedArray = new double[array.length];
		
		for(int i = 0; i < array.length; i++){
			if(rd.nextBoolean()){
				mutatedArray[i] = array[i] + ((rd.nextDouble() - 0.5) * 1.1);
			}
			else{
				mutatedArray[i] = array[i] - ((rd.nextDouble() - 0.5) * 1.1);
			}
		}
		
		return mutatedArray;
	}
	
	
}
