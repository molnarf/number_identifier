import java.util.ArrayList;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;


public class Conv_Layer {

	public String activation_function;
	public Pooling_Layer nextPoolingLayer;
	public Pooling_Layer previousLayer;
	
	public int h_input;
	public int w_input;
	public int numFilters;
	public int filter_size;
	public int outputsize;
	public double learning_rate = 0.00001;
	
	
	public ArrayList<double[][]> filters;
	public ArrayList<double[][]> biases;
	public ArrayList<double[][]> output;	// featuremaps 
	public ArrayList<double[][]> errors;
	public ArrayList<double[][]> input;
	public ArrayList<double[][]> paddedMatrices;
	

	private boolean useMatrixImplementation = false;
	public  ArrayList<RealMatrix> filters_matrix;
	public  ArrayList<RealMatrix> biases_matrix;
	public  ArrayList<RealMatrix> output_matrix;
	public  ArrayList<RealMatrix> errors_matrix;
	public  ArrayList<RealMatrix> input_matrix;
	public  ArrayList<RealMatrix> paddedMatrices_matrix;

	
	
	/**
	 * @param numFilters
	 * @param filter_size width and height of the filter
	 * @param activation_function
	 */
	public Conv_Layer(int numFilters, int filter_size, String activation_function){
		if(this.useMatrixImplementation){
			this.filters_matrix = new ArrayList<>();
			this.biases_matrix = new ArrayList<>();
			this.output_matrix = new ArrayList<>();
			this.errors_matrix = new ArrayList<>();
			this.input_matrix = new ArrayList<>();
			this.paddedMatrices_matrix = new ArrayList<>();
		}
		else{
			this.numFilters = numFilters;
			this.filter_size = filter_size;
			this.activation_function = activation_function;
	
			
			this.filters = new ArrayList<>();
			this.output = new ArrayList<>(numFilters);
			this.biases = new ArrayList<>();
		}
		// NOTE: filters and biases get initialized when calling the set nextConvLayer function in the pooling layer class
	}
	
	/**
	 * @param numFilters
	 * @param filter_size width and height of the filter
	 * @param activation_function
	 * @param h_input height of the input matrix
	 * @param w_input width of the input matrix
	 */
	public Conv_Layer(int numFilters, int filter_size, String activation_function, int h_input, int w_input){
		this(numFilters, filter_size, activation_function);
		this.h_input = h_input;
		this.w_input = w_input;
		this.outputsize = numFilters * h_input * w_input;

		initialize_filters();
		initialize_biases();
	}
	
	
	
	
	
	
	public void feedForward(ArrayList<double[][]> inputFeatureMaps){	// input are the pixels of a picture for example
		if(this.useMatrixImplementation){
			ArrayList<RealMatrix> inputFeatureMaps_matrix = new ArrayList<>();
			inputFeatureMaps_matrix.add(new Array2DRowRealMatrix(inputFeatureMaps.get(0)));
			feedForward_matrix(inputFeatureMaps_matrix);
			return;
		}
		this.output.clear();
		this.input = inputFeatureMaps;
 		for(int i = 0; i<filters.size(); i++){
			double[][] featureMap = applyFilterAndBias(inputFeatureMaps, filters.get(i), biases.get(i));
			featureMap = applyActivationFunction(featureMap);
			this.output.add(featureMap);
		}
		
		this.nextPoolingLayer.feedForward(MyUtils.copyDoubleArrayList(this.output));
	}
	
	
	public void feedForward_matrix(ArrayList<RealMatrix> inputFeatureMaps){	// input are the pixels of a picture for example
		this.output_matrix.clear();
		this.input_matrix = inputFeatureMaps;
 		for(int i = 0; i<filters_matrix.size(); i++){
			RealMatrix featureMap = applyFilterAndBias(inputFeatureMaps, filters_matrix.get(i), biases_matrix.get(i));
			featureMap = applyActivationFunction(featureMap);
			this.output_matrix.add(featureMap);
		}
		
		this.nextPoolingLayer.feedForward_matrix((this.output_matrix));
	}
	
	
	

	public double[][] applyFilterAndBias(ArrayList<double[][]> inputFeatureMaps, double[][] filter, double[][] bias){
		if(filter.length % 2 == 0){
			throw new RuntimeException("Please use an uneven filter size\n Current filter size: " + filter.length);
		}
		int padding_size = (filter.length - 1) / 2;
		int height = inputFeatureMaps.get(0).length;
		int width = inputFeatureMaps.get(0)[0].length;
		double[][] filteredMatrix = new double[height][width];
		ArrayList<double[][]> paddedMatrices = new ArrayList<>();
		
		for(int i = 0; i<inputFeatureMaps.size(); i++){
			paddedMatrices.add(new double[height + padding_size*2][width + padding_size*2]);
		}
		
		this.paddedMatrices = paddedMatrices;
		
		// copy content of input matrices to paddedMatrices
		for(int m = 0; m<inputFeatureMaps.size(); m++){
			double[][] paddedMatrix = paddedMatrices.get(m);
			double[][] matrix = inputFeatureMaps.get(m);
			for(int i = 0; i<height; i++){
				for(int j = 0; j<width; j++){
					paddedMatrix[i+padding_size][j + padding_size] += matrix[i][j];
				}
			}
		}
		
		// run over the matrices and apply the filter
		for(int i = 0; i<height; i++){
			for(int j = 0; j<width; j++){
				double filteredValue = 0;
				for(double[][] paddedMatrix : paddedMatrices){
					for(int a = 0; a<filter.length; a++){
						for(int b = 0; b<filter[0].length; b++){
							filteredValue += paddedMatrix[i+a][j+b] * filter[a][b];
						}
					}
					filteredMatrix[i][j] = filteredValue + bias[i][j];
				}
			}
		}
		
		return filteredMatrix;
	}
	
	
	public RealMatrix applyFilterAndBias(ArrayList<RealMatrix> inputFeatureMaps, RealMatrix filter, RealMatrix bias){
		if(filter.getColumnDimension() % 2 == 0){
			throw new RuntimeException("Please use an uneven filter size\n Current filter size: " + filter.getColumnDimension());
		}
		int padding_size = (filter.getColumnDimension() - 1) / 2;
		int height = inputFeatureMaps.get(0).getRowDimension();
		int width = inputFeatureMaps.get(0).getColumnDimension();
		Array2DRowRealMatrix filteredMatrix = new Array2DRowRealMatrix(new double[height][width]);
		ArrayList<RealMatrix> paddedMatrices = new ArrayList<>();
		
		for(int i = 0; i<inputFeatureMaps.size(); i++){
			RealMatrix paddedMatrix = new Array2DRowRealMatrix(new double[height + padding_size*2][width + padding_size*2]);
			paddedMatrix.setSubMatrix(inputFeatureMaps.get(i).getData(), padding_size, padding_size);
			paddedMatrices.add(paddedMatrix);
		}
		
		for(RealMatrix paddedMatrix : paddedMatrices){
			for(int i = 0; i<height; i++){
				for(int j = 0; j<width; j++){
					filteredMatrix.setEntry(i, j, sum(filter.multiply(paddedMatrix.getSubMatrix(i, i+padding_size, j, j+padding_size))));
				}
			}
		}
		this.paddedMatrices_matrix = paddedMatrices;
		
		return filteredMatrix;
	}
	
	
	
	
	public double[][] applyActivationFunction(double[][] matrix) {
		double[][] output = new double[matrix.length][matrix[0].length];
				
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (this.activation_function.equals("sigmoid")) {
					output[i][j] = MyUtils.sigmoid(matrix[i][j]);
				}
				if (this.activation_function.equals("relu")) {
					output[i][j] = MyUtils.relu(matrix[i][j]);
				}
				if (this.activation_function.equals("leaky_relu")) {
					output[i][j] = MyUtils.leaky_relu(matrix[i][j]);
				}
	
			}
		}
		
		return output;
	}
	
	
	public RealMatrix applyActivationFunction(RealMatrix matrix) {
		Array2DRowRealMatrix output = new Array2DRowRealMatrix(new double[matrix.getRowDimension()][matrix.getColumnDimension()]);
				
		for (int i = 0; i < matrix.getRowDimension(); i++) {
			for (int j = 0; j < matrix.getColumnDimension(); j++) {
				if (this.activation_function.equals("sigmoid")) {
					output.setEntry(i, j, MyUtils.sigmoid(matrix.getEntry(i, j)));
				}
				if (this.activation_function.equals("relu")) {
					output.setEntry(i, j, MyUtils.relu(matrix.getEntry(i, j)));
				}
				if (this.activation_function.equals("leaky_relu")) {
					output.setEntry(i, j, MyUtils.leaky_relu(matrix.getEntry(i, j)));
				}
	
			}
		}
		
		return output;
	}
	
	
	
	public void calculate_error(){
		this.errors = nextPoolingLayer.errors;
	}
	
	
	
	public void adjustFiltersAndBiases(){
		if(this.useMatrixImplementation){
			ArrayList<RealMatrix> derivatives = calculateDerivatives_matrix();
			ArrayList<RealMatrix> deltas = getDeltaBiasesAndSetNewBiases_matrix(derivatives);
			setNewWeights_matrix(deltas); 
			return;
		}
		
		ArrayList<double[][]> derivatives = calculateDerivatives();	//
		ArrayList<double[][]> deltas = getDeltaBiasesAndSetNewBiases(derivatives); //
		setNewWeights(deltas); //
		//RealMatrix delta_weights = this.error.mapMultiply(learning_rate).ebeMultiply(derivative).outerProduct(a);
	}
	
	/**
	 * deltas have size h_output x w_output and there should be numFilters many
	 * deltas
	 * 
	 * @param deltas
	 */
	public void setNewWeights(ArrayList<double[][]> deltas) {

		for (int d = 0; d < deltas.size(); d++) {	// for every output matrix there is one delta and thus one filter to adjust
			double[][] delta = deltas.get(d);
			double[][] delta_filter = new double[filter_size][filter_size];

			for (double[][] a : paddedMatrices) {	// we need to consider all input matrices

				for (int i = 0; i < delta.length; i++) {
					for (int j = 0; j < delta[0].length; j++) {
						for (int x = 0; x < filter_size; x++) {
							for (int y = 0; y < filter_size; y++) {
								delta_filter[x][y] += delta[i][j] * a[i + x][j + y];
							}
						}
					}
				}
			}
			
			double[][] newFilter = MyUtils.addMatrices(filters.get(d), delta_filter);
			this.filters.set(d, newFilter);
		}
	}
	
	
	public void setNewWeights_matrix(ArrayList<RealMatrix> deltas) {

		for (int d = 0; d < deltas.size(); d++) {	// for every output matrix there is one delta and thus one filter to adjust
			RealMatrix delta = deltas.get(d);
			RealMatrix delta_filter = new Array2DRowRealMatrix(new double[filter_size][filter_size]);

			for (RealMatrix a : paddedMatrices_matrix) {	// we need to consider all input matrices

				for (int i = 0; i < delta.getRowDimension(); i++) {
					for (int j = 0; j < delta.getColumnDimension(); j++) {
						for (int x = 0; x < filter_size; x++) {
							for (int y = 0; y < filter_size; y++) {
								delta_filter.setEntry(x, y, delta_filter.getEntry(x, y) + delta.getEntry(i, j) * a.getEntry(i+x, j+y));
							}
						}
					}
				}
			}
			
			RealMatrix newFilter = filters_matrix.get(d).add(delta_filter);
			this.filters_matrix.set(d, newFilter);
		}
	}
	
	
	
	/**
	 * calculates the delta biases, which are then uses to calculate the delta filters
	 * Also the method uses the calculated delta biases to already set the new biases
	 * @param derivatives
	 * @return
	 */
	public ArrayList<double[][]> getDeltaBiasesAndSetNewBiases(ArrayList<double[][]> derivatives){
		ArrayList<double[][]> delta_biases = new ArrayList<>();
		for(int b = 0; b<derivatives.size(); b++){
			double[][] derivative = derivatives.get(b);
			double[][] bias = biases.get(b);
			double[][] error = errors.get(b);
			
			double[][] detla_bias = MyUtils.mult_Mat_with_Mat(MyUtils.mult_Mat_with_double(error, learning_rate), derivative);
			double[][] new_bias = MyUtils.addMatrices(bias, detla_bias);
			
			delta_biases.add(detla_bias);
			this.biases.set(b, new_bias);
		}
		
		return delta_biases;
	}
	
	
	
	public ArrayList<RealMatrix> getDeltaBiasesAndSetNewBiases_matrix(ArrayList<RealMatrix> derivatives){
		ArrayList<RealMatrix> delta_biases = new ArrayList<>();
		for(int b = 0; b<derivatives.size(); b++){
			RealMatrix derivative = derivatives.get(b);
			RealMatrix bias = biases_matrix.get(b);
			RealMatrix error = errors_matrix.get(b);
			
			RealMatrix detla_bias = new Array2DRowRealMatrix(MyUtils.mult_Mat_with_Mat(error.scalarMultiply(learning_rate).getData(), derivative.getData()));

			RealMatrix new_bias = bias.add(detla_bias);
			
			delta_biases.add(detla_bias);
			this.biases_matrix.set(b, new_bias);
		}
		
		return delta_biases;
	}
	
	
	public ArrayList<double[][]> calculateDerivatives(){
		ArrayList<double[][]> derivatives = new ArrayList<>();
		
		for(int m = 0; m<this.errors.size(); m++){
			double[][] error = this.errors.get(m);
			double[][] derivative = new double[error.length][error[0].length];
			
			for(int i = 0; i<derivative.length; i++){
				for(int j = 0; j<derivative[0].length; j++){
					if(this.activation_function.equals("sigmoid")){
						derivative[i][j] = MyUtils.derivative_sigmoid(output.get(m)[i][j]);	// i use sigmoid here 
					}
					if(this.activation_function.equals("relu")){
						derivative[i][j] = MyUtils.derivative_relu(output.get(m)[i][j]);
					}
					if(this.activation_function.equals("leaky_relu")){
						derivative[i][j] = MyUtils.derivative_leaky_relu(output.get(m)[i][j]);
					}
				}
			}
			
			derivatives.add(derivative);
		}
		
		return derivatives;
	}
	
	
	
	public ArrayList<RealMatrix> calculateDerivatives_matrix(){
		ArrayList<RealMatrix> derivatives = new ArrayList<>();
		
		for(int m = 0; m<this.errors_matrix.size(); m++){
			RealMatrix error = this.errors_matrix.get(m);
			Array2DRowRealMatrix derivative = new Array2DRowRealMatrix(new double[error.getRowDimension()][error.getColumnDimension()]);
			
			for(int i = 0; i<derivative.getRowDimension(); i++){
				for(int j = 0; j<derivative.getColumnDimension(); j++){
					if(this.activation_function.equals("sigmoid")){
						derivative.setEntry(i, j, MyUtils.derivative_sigmoid(output_matrix.get(m).getEntry(i, j)));	// i use sigmoid here 
					}
					if(this.activation_function.equals("relu")){
						derivative.setEntry(i, j, MyUtils.derivative_relu(output_matrix.get(m).getEntry(i, j)));
					}
					if(this.activation_function.equals("leaky_relu")){
						derivative.setEntry(i, j, MyUtils.derivative_leaky_relu(output_matrix.get(m).getEntry(i, j)));
					}
				}
			}
			
			derivatives.add(derivative);
		}
		
		return derivatives;
	}
	
	
	
	public void initialize_filters(){
		NormalDistribution norm = new NormalDistribution(0, 0.01);

		for(int k = 0; k<numFilters; k++){
			double[][] filter = new double[filter_size][filter_size];
			for(int i = 0; i<filter_size; i++){
				for(int j = 0; j<filter_size; j++){
					filter[i][j] = norm.sample();
				}
			}
			if(this.useMatrixImplementation){
				this.filters_matrix.add(new Array2DRowRealMatrix(filter));
			}
			else{
				this.filters.add(filter);
			}
		}
	}
	
	
	public void initialize_biases(){

		for(int k = 0; k<numFilters; k++){
			double[][] bias = new double[h_input][w_input];
			if(this.useMatrixImplementation){
				this.biases_matrix.add(new Array2DRowRealMatrix(bias));
			}
			else{
				this.biases.add(bias);
			}
		}
	}
	
	
	public int calculateOutputsize(){
		this.outputsize = numFilters * h_input * w_input;
		return this.outputsize;
	}
	
	
	public double sum(RealMatrix m){
		double sum = 0;
		for(int i = 0; i<m.getRowDimension(); i++){
			for(int j = 0; j<m.getColumnDimension(); j++){
				sum += m.getEntry(i, j);
			}
		}
		return sum;
	}
	
	
	public void setNextPoolingLayer(Pooling_Layer pool_layer){
		this.nextPoolingLayer = pool_layer;
		pool_layer.previousLayer = this;
		pool_layer.numMaps = this.numFilters;
		pool_layer.h_input = this.h_input;
		pool_layer.w_input = this.w_input;
		pool_layer.h_output = pool_layer.h_input / pool_layer.size;
		pool_layer.w_output = pool_layer.w_input / pool_layer.size;
		
	}
	
}
