import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.math3.linear.ArrayRealVector;

public class CNN extends NN{

	public Conv_Layer firstConvLayer;
	public Pooling_Layer lastPoolLayer;
	public ArrayList<double[][]> training_images;

	
	public CNN(int[] neurons_in_layers, String activation_function){
		super(neurons_in_layers, activation_function);
		initLayersRandomly();
	}
	
	
	
	public void addConvAndPoolingLayers(ArrayList<Conv_Layer> conv_layers, ArrayList<Pooling_Layer> pooling_layers){
		if(conv_layers.size() != pooling_layers.size()){
			throw new RuntimeException("The number of convolutional and pooling layers should match");
		}
		this.firstConvLayer = conv_layers.get(0);
		
		Pooling_Layer currentPoolLayer = null;
		for(int i = 0; i<conv_layers.size(); i++){
			Conv_Layer convLayer = conv_layers.get(i);
			if(currentPoolLayer != null){
				currentPoolLayer.setNextConvLayer(convLayer);
			}
			Pooling_Layer pool_layer = pooling_layers.get(i);
			convLayer.setNextPoolingLayer(pool_layer);
			currentPoolLayer = pooling_layers.get(i);
		}
		this.lastPoolLayer = currentPoolLayer;
		currentPoolLayer.nextLayer = this.layers.get(0);
		System.out.println("Output size of the last pooling layer: " + currentPoolLayer.calculateOutputsize());
	}

	
	public void train(MnistManager dataset) throws IOException{
		readInDataset(dataset);
		
		Random rd = new Random();
		int pos = 0;
		int right = 0;
			while(pos < training_images.size()){
				int nr = rd.nextInt(training_images.size());
				int correctNeuron = training_labels.get(nr);
				double[] correct_output = new double[10];
				correct_output[correctNeuron] = 1;
				double[][] image = training_images.get(nr);

				train_instance(image, correct_output);
				
				if(correctNeuron == getMostActiveNeuron()){
					right++;
				}
				
				pos++;
			}
			System.out.println("Right : " + right);
			System.out.println("Wrong : " + (pos - right));
	}
	
	public void readInDataset(MnistManager dataset){
		MnistImageFile images = dataset.getImages();
		MnistLabelFile labels = dataset.getLabels();
		
		if(this.training_images == null){
			System.out.println("Reading in images");
			this.training_images = new ArrayList<>();
			this.training_labels = new ArrayList<>();
			for(int i = 0 ; i < training_size; i++){
				try {
					double[][] image = MyUtils.intToDoubleArray(images.readImage());
					training_images.add(image);
					training_labels.add(labels.readLabel());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Images have been read in");
		}
	}
	
	
	public void train_instance(double[][] input, double[] correct_output){
		ArrayList<double[][]> featureMap = new ArrayList<>();
		featureMap.add(input);
		
		firstConvLayer.feedForward(featureMap);
		//calculate errors in the second last hidden layer
		Layer lastLayer = layers.get(layers.size()-2);
		lastLayer.error = new ArrayRealVector(getErrorsForInstance(correct_output));
		lastLayer.adjust_weights_and_biases();

		//calculate errors in hidden layers through backpropagation
		// adjust the weights and biases
		for(int i = layers.size() - 3; i >= 0; i--){
			Layer layer = layers.get(i);
			layer.calculate_error();
			layer.adjust_weights_and_biases();
		}
		
		Pooling_Layer currentPoolLayer = this.lastPoolLayer;
		while(currentPoolLayer != null){
			currentPoolLayer.calculate_error();
			Conv_Layer currentConvLayer = currentPoolLayer.previousLayer;
			currentConvLayer.calculate_error();
			currentConvLayer.adjustFiltersAndBiases();
			currentPoolLayer = currentConvLayer.previousLayer;
		}
	}
	
	
	/**
	 * returns the most active neuron in the last layer, given the input
	 * @param input
	 * @return
	 */
	public int evaluateInstance(double[][] input){
		ArrayList<double[][]> inputFeatureMap = new ArrayList<>();
		inputFeatureMap.add(input);
		firstConvLayer.feedForward(inputFeatureMap);
		
		Layer lastLayer = layers.get(layers.size()-1);
		
		return lastLayer.getMostActiveNeuron();
	}
	
}
