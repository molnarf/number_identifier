import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javax.imageio.ImageIO;


public class Main {

	public static void main(String[] args) {
		try {
			if(true){
//				xor_example();
			}
			if(true){
//				return;
			}
		
			MnistManager m = new MnistManager("C:/Users/Florian/Documents/Testdatein/NN images/train-images-idx3-ubyte/train-images.idx3-ubyte", "C:/Users/Florian/Documents/Testdatein/NN images/train-labels-idx1-ubyte/train-labels.idx1-ubyte");
			
			
			CNN cnn = generateCNN();
//			ArrayList<double[][]> input = new ArrayList<>();
//			input.add(MyUtils.intToDoubleArray(m.getImages().readImage()));
//			cnn.firstConvLayer.feedForward(input);
//			cnn.train(m);
			
//			NN nn = new NN(new int[]{784, 32, 10}, "leaky_relu");
//			nn.initLayersRandomly();
//			NN nn = new NN("C:/Users/Florian/Documents/Testdatein/NN images/savedNNs/relu_128_50_1it", "relu");
//			
//
			for(int i = 0; i<200; i++){
				System.out.println("\nIteration : " + (i+1));
				m = new MnistManager("C:/Users/Florian/Documents/Testdatein/NN images/train-images-idx3-ubyte/train-images.idx3-ubyte", "C:/Users/Florian/Documents/Testdatein/NN images/train-labels-idx1-ubyte/train-labels.idx1-ubyte");
//				nn.train(m);
				cnn.train(m);
			}
//			nn.saveNN("C:/Users/Florian/Documents/Testdatein/NN images/savedNNs/relu_128_50_1it");
			
			
//			evaluate_testdata(nn);
			displayMyImages(cnn);
			displayImages(cnn);
			 
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
	public static CNN generateCNN(){
		CNN cnn = new CNN(new int[]{490, 32, 10}, "leaky_relu");
		Conv_Layer c1 = new Conv_Layer(32, 5, "leaky_relu", 28, 28);
		Pooling_Layer p1 = new Pooling_Layer(2);
		Conv_Layer c2 = new Conv_Layer(10, 3, "leaky_relu");
		Pooling_Layer p2 = new Pooling_Layer(2);
		ArrayList<Conv_Layer> conv_layers = new ArrayList<>(Arrays.asList(c1, c2));
		ArrayList<Pooling_Layer> pool_layers = new ArrayList<>(Arrays.asList(p1, p2));
		cnn.addConvAndPoolingLayers(conv_layers, pool_layers);
		
		cnn.training_size = 1000;
		return cnn;
	}
	
	public static void displayMyImages(NN nn) throws IOException{
		ImageDisplayer d = new ImageDisplayer();
		// evaluating my images
		for(int nr = 0; nr <= 9; nr++){
			String filename = "C:/Users/Florian/Documents/Testdatein/NN images/MyNumms/pixil-frame-0 (" + nr + ").png";
			BufferedImage myImage = ImageIO.read(new File(filename));
			
			int[][] pixels = new int[28][28];
			
			for( int i = 0; i < 28; i++){
				for( int j = 0; j < 28; j++){
					pixels[j][i] = myImage.getRGB( i, j );
					int red   = (pixels[j][i] >>> 16) & 0xFF;
					int green = (pixels[j][i] >>>  8) & 0xFF;
					int blue  = (pixels[j][i] >>>  0) & 0xFF;
					pixels[j][i] = (int) ((red * 0.2126f + green * 0.7152f + blue * 0.0722f));
				}
			}
			int prediction;
			if(nn instanceof CNN){
				prediction = ((CNN) nn).evaluateInstance(MyUtils.intToDoubleArray(pixels));
			}
			else{
				prediction = nn.evaluateInstance(MyUtils.flattenMatrix(MyUtils.intToDoubleArray(pixels)));
			}
			d.displayImage(myImage, "Prediction: " + prediction);
		}
	}
	

		
	public static void displayImages(NN nn) throws IOException{
		MnistManager m = new MnistManager("C:/Users/Florian/Documents/Testdatein/NN images/train-images-idx3-ubyte/train-images.idx3-ubyte", "C:/Users/Florian/Documents/Testdatein/NN images/train-labels-idx1-ubyte/train-labels.idx1-ubyte");
		ImageDisplayer d = new ImageDisplayer();
	
		MnistImageFile images = m.getImages();
		
		for(int i = 0 ; i < 50; i++){
			int[][] x = images.readImage();
	
			BufferedImage img = MyUtils.arrayToImage(x);
			int prediction;
			if(nn instanceof CNN){
				prediction = ((CNN) nn).evaluateInstance(MyUtils.intToDoubleArray(x));
			}
			else{
				prediction = nn.evaluateInstance(MyUtils.flattenMatrix(MyUtils.intToDoubleArray(x)));
			}

			d.displayImage(img, "Prediction: " + prediction);
		}
	}

	
	public static void evaluate_testdata(NN nn){
		try {
			MnistManager m = new MnistManager("C:/Users/Florian/Documents/Testdatein/NN images/t10k-images-idx3-ubyte/t10k-images.idx3-ubyte", "C:/Users/Florian/Documents/Testdatein/NN images/t10k-labels-idx1-ubyte/t10k-labels.idx1-ubyte");

			nn.evaluateDataset(m);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	

	public static void xor_example(){
		try{
		NN nn = new NN(new int[]{2,10,10,1}, "relu");
		nn.initLayersRandomly();
		
		ArrayList<double[]> inputs = new ArrayList<double[]>();
		ArrayList<double[]> outputs = new ArrayList<double[]>();
		
		
		inputs.add(new double[]{0, 0});
		inputs.add(new double[]{1, 0});
		inputs.add(new double[]{0, 1});
		inputs.add(new double[]{1, 1});
		
		outputs.add(new double[]{0});
		outputs.add(new double[]{1});
		outputs.add(new double[]{1});
		outputs.add(new double[]{0});
		
		
		Random rd = new Random();
		for(int i = 0; i<100000; i++){
			int nr = rd.nextInt(4);
			nn.train_instance(inputs.get(nr), outputs.get(nr));
		}
		
		
		
		nn.process_instance(inputs.get(0));
		nn.process_instance(inputs.get(1));
		nn.process_instance(inputs.get(2));
		nn.process_instance(inputs.get(3));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
