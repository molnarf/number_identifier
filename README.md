#### Train a feedforward neural network or a convolutional neural network to identify and predict handwritten numbers.

The neural networks are implemented from scratch in java.
Based on the mnist dataset.

<img src="images/screenshot.JPG" width="700">
